#!/bin/bash

TEXNAME="OverlordVol14"
CURRENT_VERSION=$(git describe --abbrev=0 --tags)
API_TOKEN=`cat .api_token`
PROJECT_ID=`cat .project_id`

function gtag() {
	git tag -a v$1 -m "$1"
}

echo "Current version: $CURRENT_VERSION"
read -p "Change version? (y/n) " RESP

if [ "$RESP" = "y" ]; then
	read -p "Enter next version (without v): " NEXT_VERSION
	gtag $NEXT_VERSION
	git push --tags
elif [ "$RESP" = "n" ]; then
	echo "Updating current release..."
else
	echo "Input must be either 'y' or 'n'."
	exit
fi

GIT_HASH_HEAD=$(git rev-parse HEAD | head -c 6)

CURRENT_VERSION=$(git describe --abbrev=0 --tags)

mkdir -p releases

rm releases/*

# DARK PDF
lualatex -synctex=1 -interaction=nonstopmode $TEXNAME.tex
mv $TEXNAME.pdf releases/\[TB\]\ $TEXNAME\ DARK\ $CURRENT_VERSION\ $GIT_HASH_HEAD.pdf

# LIGHT PDF
lualatex -synctex=1 -interaction=nonstopmode "\def\isWHITE{1} \input{$TEXNAME.tex}"
mv $TEXNAME.pdf releases/\[TB\]\ $TEXNAME\ LIGHT\ $CURRENT_VERSION\ $GIT_HASH_HEAD.pdf

# EPUB
pandoc $TEXNAME.tex -t epub3 --css resources/epub.css --number-sections --toc --toc-depth=2 -o releases/\[TB\]\ $TEXNAME\ $CURRENT_VERSION\ $GIT_HASH_HEAD.epub

if [ -z $PROJECT_ID ] || [ -z $API_TOKEN ]; then
	exit
fi

PATHS=`readlink -f releases/*`
RELEASE_STR=""

while read -r line; do
	JSON_OUTPUT=$(curl --request POST --header "Private-Token: $API_TOKEN" --form "file=@$line" "https://gitlab.com/api/v4/projects/$PROJECT_ID/uploads")
	RELEASE_STR+=$(echo $JSON_OUTPUT | jq -r '.markdown')
	RELEASE_STR+='\n\n'
done <<< "$PATHS"

if [ "$RESP" = "y" ]; then
	curl --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $API_TOKEN" \
	     --data '{ "name": "'"$CURRENT_VERSION"'", "tag_name": "'"$CURRENT_VERSION"'", "description": "'"$RELEASE_STR"'" }' \
	     --request POST https://gitlab.com/api/v4/projects/$PROJECT_ID/releases
else
	curl --header 'Content-Type: application/json' --request PUT --data '{"name": "'"$CURRENT_VERSION"'", "description": "'"$RELEASE_STR"'"}' --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/releases/$CURRENT_VERSION"
fi
